import React, {useState, useEffect} from 'react';
import {
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	Image,
	TextInput,
	ActivityIndicator,
} from 'react-native';
import {Icon, Status, PopUp} from '../../shared/components';
import {styles} from './style_transactionList';
import {Rupiah, convertDateToString, checkCapital} from '../../shared/function';
import {useSelector} from 'react-redux';
import {AppStyle, wp, hp} from '../../shared/style';
const {color} = AppStyle;

export const TransactionList = (props) => {
	const [modalVisible, setModalVisible] = useState(false);
	const [filter, setFilter] = useState('URUTAN');
	const [data, setData] = useState(transactionData);
	const transactionData = useSelector((state) => state.dataFlip.data);

	useEffect(() => {
		if (!data) {
			props.getData();
		}
	}, []);

	useEffect(() => {
		if (transactionData) {
			setData(transactionData);
		}
	}, [transactionData]);

	// HandleClick Filter
	const filterHandle = (e) => {
		if (e == 'Nama Z-A') {
			const dataFilter = transactionData.sort((a, b) => {
				const nameA = a.beneficiary_name.toUpperCase();
				const nameB = b.beneficiary_name.toUpperCase();
				if (nameA > nameB) {
					return -1;
				}
				if (nameA < nameB) {
					return 1;
				}
				return 0;
			});
			setData(dataFilter);
		} else if (e == 'Nama A-Z') {
			const dataFilter = transactionData.sort((a, b) => {
				const nameA = a.beneficiary_name.toUpperCase();
				const nameB = b.beneficiary_name.toUpperCase();
				if (nameA < nameB) {
					return -1;
				}
				if (nameA > nameB) {
					return 1;
				}
				return 0;
			});
			setData(dataFilter);
		} else if (e == 'Tanggal Terbaru') {
			const dataFilter = transactionData.sort((a, b) => {
				const nameA = a.created_at;
				const nameB = b.created_at;
				if (nameA > nameB) {
					return -1;
				}
				if (nameA < nameB) {
					return 1;
				}
				return 0;
			});
			setData(dataFilter);
		} else if (e == 'Tanggal Terlama') {
			const dataFilter = transactionData.sort((a, b) => {
				const nameA = a.created_at;
				const nameB = b.created_at;
				if (nameA < nameB) {
					return -1;
				}
				if (nameA > nameB) {
					return 1;
				}
				return 0;
			});
			setData(dataFilter);
		}
	};

	// Handle Searchbar
	const searchFilterFunction = (text) => {
		const newData = transactionData.filter((item) => {
			const itemData = `${item.beneficiary_bank.toUpperCase()}   
   			 ${item.beneficiary_name.toUpperCase()} ${item.sender_bank.toUpperCase()} ${
				item.amount
			} ${item.id}`;
			const textData = text.toUpperCase();
			return itemData.indexOf(textData) > -1;
		});
		setData(newData);
	};

	// Handle Modal
	const handleClickModal = (e) => {
		setFilter(e);
		filterHandle(e);
	};

	return (
		<View style={styles.container}>
			<View style={styles.search}>
				<Icon name="search" />
				<TextInput
					placeholder="Cari nama ,bank, atau nominal"
					onChangeText={(text) => searchFilterFunction(text)}
					style={styles.formInput}
				/>
				<TouchableOpacity
					style={styles.flex}
					onPress={() => setModalVisible(!modalVisible)}>
					<Text style={styles.filterText}>{filter}</Text>
					<Icon name="downArrow" />
				</TouchableOpacity>
			</View>
			<PopUp
				data={handleClickModal}
				visible={modalVisible}
				onPress={() => setModalVisible(!modalVisible)}
			/>
			{props.isLoading ? (
				<ActivityIndicator
					size="large"
					color={color.red}
					style={styles.loading}
				/>
			) : (
				<ScrollView showsVerticalScrollIndicator={false} style={styles.scroll}>
					{data &&
						data.map((item, index) => (
							<TouchableOpacity
								onPress={() => props.navigation.navigate('Detail', item)}
								key={index}
								style={
									item.status === 'SUCCESS'
										? styles.card
										: {...styles.card, borderLeftColor: color.red}
								}>
								<View style={styles.flexCard}>
									<View>
										<View style={styles.flex}>
											<Text style={styles.txtBank}>
												{checkCapital(item.beneficiary_bank)}
											</Text>
											<Icon name="arrowIcon" />
											<Text style={styles.txtBank}>
												{' '}
												{checkCapital(item.sender_bank)}
											</Text>
										</View>
										<Text style={styles.txtName}>
											{item.beneficiary_name.toUpperCase()}
										</Text>
										<View style={styles.flex}>
											<Text style={styles.txtAmount}>
												Rp{Rupiah(item.amount)}
											</Text>
											<Icon name="dot" />
											<Text style={styles.txtAmount}>
												{convertDateToString(
													item.status == 'SUCCESS'
														? item.completed_at
														: item.created_at,
												)}
											</Text>
										</View>
									</View>
									<Status type={item.status} />
								</View>
							</TouchableOpacity>
						))}
				</ScrollView>
			)}
		</View>
	);
};
