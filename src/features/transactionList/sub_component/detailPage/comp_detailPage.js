import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {styles} from './style_detailPage';
import {Icon, Status, PopUp} from '../../../../shared/components';
import {AppStyle, wp, fp, hp} from '../../../../shared/style';
import Clipboard from '@react-native-clipboard/clipboard';
import {
	Rupiah,
	convertDateToString,
	checkCapital,
} from '../../../../shared/function';
const {color, font, fontSize} = AppStyle;

const DetailPage = (props) => {
	const item = props.route.params;

	const copyToClipboard = () => {
		Clipboard.setString(item.id)
		alert('Copied to clipboard')
	}
	return (
		<View style={styles.container}>
			<View style={styles.flex}>
				<Text style={{...styles.idTransaksi, paddingVertical: hp(2)}}>
					ID TRANSAKSI:#{item.id}
				</Text>
				<TouchableOpacity style={{justifyContent:'center',alignSelf:'center'}} onPress={copyToClipboard}>
					<Icon name="copy"  />
				</TouchableOpacity>
			</View>
			<View style={styles.title}>
				<Text style={styles.idTransaksi}>DETAIL TRANSAKSI</Text>
				<TouchableOpacity onPress={() => props.navigation.navigate('Home')}>
					<Text style={styles.closeBtn}>Tutup</Text>
				</TouchableOpacity>
			</View>
			<View style={{...styles.flex, paddingVertical: hp(2)}}>
				<Text style={styles.txtBank}>
					{checkCapital(item.beneficiary_bank)}
				</Text>
				<Icon name="arrowIcon" />
				<Text style={styles.txtBank}> {checkCapital(item.sender_bank)}</Text>
			</View>
			<View style={{...styles.flex, paddingVertical: hp(2)}}>
				<View style={{flex: 1}}>
					<Text style={styles.txtDetail}>
						{item.beneficiary_name.toUpperCase()}
					</Text>
					<Text style={styles.txtDetail}>{item.account_number}</Text>
				</View>
				<View style={{flex: 0.5}}>
					<Text style={styles.txtDetail}>NOMINAL</Text>
					<Text style={styles.txtDetail}>Rp{Rupiah(item.amount)}</Text>
				</View>
			</View>
			<View style={{...styles.flex, paddingVertical: hp(2)}}>
				<View style={{flex: 1}}>
					<Text style={styles.txtDetail}>BERITA TRANSFER</Text>
					<Text style={styles.txtDetail}>{item.remark}</Text>
				</View>
				<View style={{flex: 0.5}}>
					<Text style={styles.txtDetail}>KODE UNIK</Text>
					<Text style={styles.txtDetail}>Rp{Rupiah(item.unique_code)}</Text>
				</View>
			</View>
			<View style={{paddingVertical: hp(2)}}>
				<Text style={styles.txtDetail}>WAKTU DIBUAT</Text>
				<Text style={styles.txtDetail}>
					{convertDateToString(item.created_at)}
				</Text>
			</View>
		</View>
	);
};

export default DetailPage;
