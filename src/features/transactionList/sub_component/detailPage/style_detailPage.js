import {StyleSheet} from 'react-native';
import {AppStyle, wp, fp, hp} from '../../../../shared/style';

const {color, font, fontSize} = AppStyle;

export const styles = StyleSheet.create({
	flex: {
		flexDirection: 'row',
	},
	txtBank: {
		fontSize: fontSize.font16,
		fontFamily: font.bold,
	},
	container: {
		padding : wp(5),
		backgroundColor:color.light,
		borderRadius:10
	},
	title:{
		flexDirection : 'row',
		justifyContent:'space-between',
		paddingVertical: hp(3),
		borderBottomWidth:3,
		borderColor: color.primary
	},
	idTransaksi : {
		fontFamily : font.bold,
		fontSize: fontSize.font16,
	},
	closeBtn : {
		fontFamily : font.regular,
		fontSize: fontSize.font16,
		color: color.red
	},
	txtDetail : {
		fontFamily : font.bold,
		fontSize: fontSize.font14,
	}
});
