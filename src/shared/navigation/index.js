import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import TransactionList from '../../features/transactionList/container_transactionList'
import DetailPage from '../../features/transactionList/sub_component/detailPage/comp_detailPage'

const Stack = createStackNavigator();

const Navigation = () => {
	return (
		<NavigationContainer>
			<Stack.Navigator>
				<Stack.Screen name="Home" component={TransactionList} options={{headerShown:false}}/>
				<Stack.Screen name="Detail" component={DetailPage} options={{headerShown:false}}/>
			</Stack.Navigator>
		</NavigationContainer>
	);
};

export default Navigation;
