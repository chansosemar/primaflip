import React, {useState} from 'react';
import {View, Text, TouchableOpacity, Modal, StyleSheet} from 'react-native';
import Icon from '../icon';
import {AppStyle, wp, fp, hp} from '../../style';

const {color, font, fontSize} = AppStyle;

const PopUp = ({visible, onPress, data}) => {
	const [active, setActive] = useState(0)
	// Simplified Render
	const listFilter = [
		{
			title: 'URUTAN',
		},
		{
			title: 'Nama A-Z',
		},
		{
			title: 'Nama Z-A',
		},
		{
			title: 'Tanggal Terbaru',
		},
		{
			title: 'Tanggal Terlama',
		},
	];

	// HandleModal
	const handleCLickModal = (item, index) => {
		setActive(index),
		data(item.title),
		setTimeout(() => {
			onPress()
		},500)
	}

	return (
		<Modal animationType="fade" transparent={true} visible={visible}>
			<View style={styles.modalContainer}>
				<View style={styles.modalCard}>
					{listFilter.map((item, index) => (
						<TouchableOpacity key={index} onPress={() => handleCLickModal(item,index)} style={styles.listFilter}>
							<Icon name={active == index ? 'radioActive' : 'radio'}/>
							<Text style={styles.txtFilter}>{item.title}</Text>
						</TouchableOpacity>
					))}
				</View>
			</View>
		</Modal>
	);
};

export default PopUp;

const styles = StyleSheet.create({
	modalContainer: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: 'rgba(100,100,100,0.8)',
	},
	modalCard: {
		backgroundColor: color.light,
		width: wp(90),
		borderRadius:5,
		paddingHorizontal: wp(3),
		paddingVertical:hp(5)
	},
	listFilter : {
		flexDirection : 'row',
		paddingVertical : hp(2),
	},
	txtFilter : {
		fontFamily : font.regular,
		fontSize: fontSize.font20,
	},

});
